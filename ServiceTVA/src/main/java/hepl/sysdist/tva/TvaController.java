package hepl.sysdist.tva;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TvaController {
    private TvaService tvaService;

    public TvaController(TvaService tvaService) {
        this.tvaService = tvaService;
    }

    @GetMapping("tva/{categoryId}")
    TVADto getTVA(@PathVariable("categoryId") Long categoryId) {
        return new TVADto(categoryId, tvaService.getTVA(categoryId));
    }
}
