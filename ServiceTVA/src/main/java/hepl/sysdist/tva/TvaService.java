package hepl.sysdist.tva;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TvaService {
    @Value("${livre}")
    private int livre;
    @Value("${autre}")
    private int autre;

    public int getTVA(Long categoryId) {
        if (categoryId == 1) {
            return livre;
        } else {
            return autre;
        }
    }
}
