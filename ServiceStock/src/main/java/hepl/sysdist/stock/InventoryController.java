package hepl.sysdist.stock;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryController {
    private InventoryService inventoryService;

    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GetMapping("/inventory/{itemId}/{quantity}")
    public boolean checkInventory(@PathVariable("itemId") Long itemId, @PathVariable("quantity") int quantity) {
        return inventoryService.checkInventory(itemId, quantity);
    }
}
