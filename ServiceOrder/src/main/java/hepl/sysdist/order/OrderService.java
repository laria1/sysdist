package hepl.sysdist.order;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Order createOrder(Long clientId, String status, double amount, List<Item> items) {
        Order order = new Order(clientId, status, amount, items);
        return orderRepository.save(order);
    }


}
