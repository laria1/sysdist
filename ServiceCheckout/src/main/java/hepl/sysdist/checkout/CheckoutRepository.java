package hepl.sysdist.checkout;

import org.springframework.data.repository.CrudRepository;

public interface CheckoutRepository extends CrudRepository<Checkout, Long> {
}
