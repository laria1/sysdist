package hepl.sysdist.item;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class ItemController {
    private ItemService itemService;

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping("/item")
    List<Item> getItems() {
        System.out.println("Dans le deuxieme service"); return itemService.getItems();
    }

    @GetMapping("/item/{itemId}")
    Optional<Item> getItem(@PathVariable("itemId") Long itemId) {
        return itemService.getItem(itemId);
    }
}
