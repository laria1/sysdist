package hepl.sysdist.cart.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Cart {
    @Id
    private Long clientId;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Entry> entries;

    public Cart() {
    }

    public Cart(Long clientId) {
        this.clientId = clientId;
        this.entries = new LinkedList<>();
    }

    public Long getClientId() {
        return clientId;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void addEntry(Entry entry) {
        entries.add(entry);
    }

    public void removeEntry(Entry entry) {
        entries.remove(entry);
    }
}
