package hepl.sysdist.front.serviceclient;


import hepl.sysdist.front.serviceclient.dto.ItemDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient("SERVICE-ITEM")
public interface ItemServiceClient {
    @RequestMapping(method = RequestMethod.GET, value = "/item", consumes = "application/json")
    List<ItemDto> getItems();
}
